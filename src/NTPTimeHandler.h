#ifndef NTPTimeHandler_h
#define NTPTimeHandler_h

#include <Arduino.h>
#include <Time.h>

// Class: NTPTimeHandler
//
// *Cares about User Timezone and Time*
//
//   - Uses NTP Serverpool for Time Synchronization
//   - As Fallback, if NTP Serverpool is not reachable(No Internet Conection etc..), Time is Synchronized with Users Phone Time
//   - For Daylight saving, the class uses POSIX Timestrings from Users Phone Timezone
class NTPTimeHandler
{
public:
    NTPTimeHandler();
    // Group: Public Methods
    // _____________________________________
    // Function: syncBySmartPhone
    // *Gets called by Starting of the Smartphone APP*
    //
    // *Changes Timezone(TZ) if needed:*
    //
    //   - PosixTZ String not yet set? -> Set PosixTZ String from Phone
    //   - PosixTZ String set in EEPROM but different from Phone and setTimezone = 2? -> Show popUp to User if he wants to change Timezone
    //   - PosixTZ String set in EEPROM but different from Phone and setTimezone = 1? -> Change Timezone
    //   - PosixTZ String set in EEPROM but different from Phone and setTimezone = 0? -> Dont change Timezone
    //   - PosixTZ String set in EEPROM and not diffrent from Phone -> No Change needed
    //
    // *Synchronize Time if needed:*
    //
    //   - Flag NTP Server Aviable in EEPROM = false? -> Set unixTime from Phone
    //   - Flag NTP Server Aviable in EEPROM = true? -> No synch needed
    //
    // *Changes NTP Server if needed:*
    //
    //   - NTP Server in EEPROM diffrent from Phone? -> Change NTP Server
    //
    // Parameters:
    //
    // unixTime - Unix Time of Phone example: 1575122607 for 11/30/2019 @ 2:03pm (UTC)
    // posixTZ - Posix Timezone String example: CET-1CEST,M3.5.0,M10.5.0/3 for Europe/Berlin see: https://github.com/nayarsystems/posix_tz_db/blob/master/zones.csv
    // setTimezone - Controlls, if Timezone gets overwritten(See Method description for Details)
    // ntpServer - NTP Server to catch Time from for Example: pool.ntp.org
    //
    // Returns:
    //
    // - 0 = Error
    // - 1 = Success
    // - 2 = Confirmation for overwritting TimeZone needed
    int syncBySmartPhone(time_t unixTime, String posixTZ, int setTimezonen, char *ntpServer);

private:
    // Group: Private Check Methods
    // _____________________________________
    // method: isPosixTZSameAsInEEPROM
    // Checks if given Posix Timezone is the same as in EEPROM
    //
    // Parameters:
    // posixTZ - Posix Timezone
    bool isPosixTZSameAsInEEPROM(String posixTZ);
    // method: isNTPServerSameAsInEEPROM
    // Checks if given NTPServer is the same as in EEPROM
    //
    // Parameters:
    // ntpServer - Posix Timezone
    bool isNTPServerSameAsInEEPROM(char *ntpServer);
    // method: isNTPServerAviable
    // Returns if NTP Server is aviable
    bool isNTPServerAviable();
    // Group: Private Get Methods
    // _____________________________________
    // method: getPosixTZ
    // Returns Posix Timezone from EEPROM
    bool getPosixTZ(char *posixTZ);
    // method: getNTPServer
    // Returns NTPServer from EEPROM
    bool getNTPServer(char *ntpServer);
    // Group: Private Write Methods
    // _____________________________________
    // method: writePosixTZEEPROMIfInitial
    // If Posix Timezone in EEPROM is initial(First time startet)
    //
    // Parameters:
    // posixTZ - Posix Timezone
    bool writePosixTZEEPROMIfInitial(String posixTZ);
    // method: writeNTPServerEEPROMIfDiffrent
    // Writes NTP Server to EEPROM
    bool writeNTPServerEEPROMIfDiffrent(String ntpServer);
};

#endif