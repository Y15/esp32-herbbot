#include "NTPTimeHandler.h"
#include "Logger.h"
#include "Storage.h"

Storage EEPROM;

NTPTimeHandler::NTPTimeHandler(){};

int NTPTimeHandler::syncBySmartPhone(time_t unixTime, String posixTZ, int setTimezone, char *ntpServer)
{
    Logger::println("---- Sync By Smartphone ----");

    writePosixTZEEPROMIfInitial(posixTZ);

    // Change Timezone if diffrent and requested by User
    if (!isPosixTZSameAsInEEPROM(posixTZ))
    {
        switch (setTimezone)
        {
        case 1:
            // PosixTZ different from Phone and setTimezone = 1 -> Change Timezone
            Logger::println("PosixTZ different from Phone, change Timezone in EEPROM");
            EEPROM.writePosixTZ(posixTZ);
            break;
        case 2:
            // PosixTZ different from Phone and setTimezone = 2 -> Show popUp to User if he wants to change Timezone
            Logger::println("PosixTZ different from Phone, show popUp to User if he wants to change Timezone");
            return 2;
        default:
            // PosixTZ different from Phone and setTimezone = 0 -> Dont change Timezone
            Logger::println("PosixTZ different from Phone, dont Change Timezone");
        }
    }

    //Change NTP Server if server is updated from User Phone
    writeNTPServerEEPROMIfDiffrent(ntpServer);
}

/////////////////////////////////////////////////////////////////
///////////////////////// Write Methods /////////////////////////
/////////////////////////////////////////////////////////////////
bool NTPTimeHandler::writePosixTZEEPROMIfInitial(String posixTZ)
{
    String posixTZFromEEPROM = "";
    EEPROM.readPosixTZ(posixTZFromEEPROM);

    if (posixTZFromEEPROM.isEmpty()) //Posix Timestamp is initial, set Timestamp from Users Phone
    {
        Logger::print("PosixTz in EEPROM is initial, set it to:");
        Logger::println(posixTZ);
        return EEPROM.writePosixTZ(posixTZ);
    }
    else
    {
        return true;
    };
}

bool NTPTimeHandler::writeNTPServerEEPROMIfDiffrent(String ntpServer)
{
    String ntpServerFromEEPROM;

    EEPROM.readNTPServer(ntpServerFromEEPROM);

    if (ntpServerFromEEPROM.equals(ntpServer))
    {
        Logger::print("NTPServer: ");
        Logger::print(ntpServer);
        Logger::println(" equals NTP Server in EEPROM, do nothing");
        return true;
    }
    else
    {
        return EEPROM.writeNTPServer(ntpServer);
    }
}

/////////////////////////////////////////////////////////////////
////////////////////////// Check Methods ////////////////////////
/////////////////////////////////////////////////////////////////
bool NTPTimeHandler::isPosixTZSameAsInEEPROM(String posixTZ)
{
    String posixTZFromEEPROM;
    EEPROM.readPosixTZ(posixTZFromEEPROM);

    return posixTZFromEEPROM.equals(posixTZ);
}

bool NTPTimeHandler::isNTPServerSameAsInEEPROM(char *ntpServer)
{
    return true;
}

bool NTPTimeHandler::isNTPServerAviable()
{
    return true;
}