#include "Storage.h"
#include "Arduino.h"
#include "Logger.h"
#include "Wire.h"

using namespace std;

//Adress of EEPROM
uint8_t device = 0x50;

int timeConfigPosition = 500;

Storage::Storage()
{
    Wire.begin(SDA, SCL, 400000);
};

/////////////////////////////////////////////////////////////////
////////////////////////// Write Funtions////////////////////////
/////////////////////////////////////////////////////////////////
template <class T>
bool Storage::writeConfig(T &value)
{
    Logger::println("Write Config");
    return write(timeConfigPosition, value);
}

bool Storage::writePosixTZ(String &posixTZ)
{
    struct timeConfig timeConfig;
    bool success;

    Logger::print("Set PosixTZ to:");
    Logger::println(String(posixTZ));
    success = readConfig(timeConfig);
    if (success == true)
    {
        strcpy(timeConfig.posixTZ, posixTZ.c_str());
        success = writeConfig(timeConfig);
    }
    return success;
}

bool Storage::writeNTPServer(String &ntpServer)
{
    struct timeConfig timeConfig;
    bool success;

    Logger::print("Set NTPServer in EEPROM to:");
    Logger::println(String(ntpServer));
    success = readConfig(timeConfig);
    if (success == true)
    {
        // strcpy(timeConfig.ntpServer, ntpServer.c_str());
        strcpy(timeConfig.posixTZ, "test");
        success = writeConfig(timeConfig);
    }

    return success;
}

template <class T>
bool Storage::write(int start, const T &value)
{
    Logger::println("Write to EEPROM");

    delay(50);

    const byte *p = (const byte *)(const void *)&value;
    Wire.beginTransmission(device);
    Wire.write((int)(start >> 8));   // MSB
    Wire.write((int)(start & 0xFF)); // LSB

    for (int i = 0; i < sizeof(value); i++)
    {
        Wire.write(*p++);
    }

    uint8_t err = Wire.endTransmission();

    if (err != 0)
    {
        Logger::print("Write Failure=");
        Logger::println(err, DEC);
        return false;
    }
    else
    {
        Logger::println("Write was successful");
        return true;
    }
}

/////////////////////////////////////////////////////////////////
////////////////////////// Read Functions ///////////////////////
/////////////////////////////////////////////////////////////////
template <class T>
bool Storage::readConfig(T &value)
{
    Logger::println("Read Config");
    return read(timeConfigPosition, value);
}

bool Storage::readNTPServer(String &ntpServer)
{
    struct timeConfig timeConfig;
    bool success;

    Logger::println("Read NTP Server");
    success = readConfig(timeConfig);
    ntpServer = timeConfig.ntpServer;

    Logger::print("NTP Server:");
    Logger::println(ntpServer);

    return success;
}

bool Storage::readPosixTZ(String &posixTZ)
{
    struct timeConfig timeConfig;
    bool success;

    Logger::println("Read PosixTZ");
    success = readConfig(timeConfig);
    posixTZ = timeConfig.posixTZ;

    Logger::print("PosixTZ:");
    Logger::println(posixTZ);

    return success;
}

template <class T>
bool Storage::read(int ee, T &value)
{
    Logger::println("Read from EEPROM");
    delay(50);
    byte *p = (byte *)(void *)&value;
    int i;
    Wire.beginTransmission(device);
    Wire.write((int)(ee >> 8));   // MSB
    Wire.write((int)(ee & 0xFF)); // LSB
    uint8_t err = Wire.endTransmission();
    Wire.requestFrom(device, sizeof(value));

    for (i = 0; i < sizeof(value); i++)
    {
        if (Wire.available())
        {
            *p++ = Wire.read();
        }
    }

    if (err != 0)
    {
        Logger::print("Read Failure=");
        Logger::println(err, DEC);
        return false;
    }
    else
    {
        Logger::println("Read was successful");
        return true;
    }
}