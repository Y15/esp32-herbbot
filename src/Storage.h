#ifndef Storage_h
#define Storage_h

#include <Arduino.h>
#include <Structs.h>
// Class: Storage
//
// Cares about Communication with EEPROM 24LC512.
// The 24LC512 Can save up to 512kBit = 64000Byte
class Storage
{
public:
    Storage();
    
    // Group: Public read Methods
    // _____________________________________
    // method: readPosixTZ
    bool readPosixTZ(String &posixTZ);
    // method: readNTPServer
    bool readNTPServer(String &ntpServer);
    
    // Group: Public write Methods
    // _____________________________________
    // method: writePosixTZ
    bool writePosixTZ(String &posixTZ);
    // method: writeNTPServer
    bool writeNTPServer(String &ntpServer);

private:
    // Group: Private Methods
    // _____________________________________
    // method: write
    // Write any Variable to external EEPROM
    //
    // Parameters:
    // start - Starting Byte
    // T - Data to write to ESP
    template <class T>
    bool write(int start, const T &value);

    // method: read
    // Read Value from EEPROM
    //
    // Parameters:
    // start - Starting Byte
    // T - Data read from EEPROM
    template <class T>
    bool read(int start, T &value);

    // method: writeConfig
    // Since the config is created with the app, we need to store it persistent in EEPROM.
    // Otherwise the user would have to reconfig the timezone etc. after restarting the HerbBot
    //
    // Parameters:
    // config - contains Fields like posixTimezone, NTPServer etc.
    template <class T>
    bool writeConfig(T &value);

    // method: readConfig
    // Read Config from ESP
    //
    // Parameters:
    // value - will contain the config after read
    template <class T>
    bool readConfig(T &value);
};

#endif