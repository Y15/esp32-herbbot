// About: timeConfig
//
// posixTZ - Contains posixTimestamp of Users Timezone.
//           example: CET-1CEST,M3.5.0,M10.5.0/3 for Europe/Berlin see: https://github.com/nayarsystems/posix_tz_db/blob/master/zones.csv
// ntpServer - Contains NTP Server of Users Phone
//             example: ch.pool.ntp.org
struct timeConfig
{
    char ntpServer[40];
    char posixTZ[40];
};