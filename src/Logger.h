#ifndef Logger_h
#define Logger_h

#include <Arduino.h>
// Class: Logger
//
// Handles logging(Serial.println, Serial.print)
// To minimize the performance impact, no log is printed to Serial in Production
class Logger
{
public:
    Logger();

    // Group: Public Methods
    // _____________________________________
    // method: println
    // Same as Serial.println but will not log in production
    //
    // Parameters:
    // message - Message to print to Serial Output as new Line
    static void println(String message);

    // method: println
    // Same as Serial.println but will not log in production
    //
    // Parameters:
    // char - Message to print
    // int - Specifies the base (format)
    static void println(unsigned char message, int = 10);

    // method: println
    // Same as Serial.println but will not log in production
    //
    // Parameters:
    // char - Message to print
    static void println(const char *);

    // method: print
    // Same as Serial.print but will not log in production
    //
    // Parameters:
    // message - Message to print to Serial Output
    static void print(String message);

    // method: begin
    // Same as Serial.begin but will not start in production
    //
    // Parameters:
    // baud - Baudrate
    static void begin(unsigned long baud);

private:
    // Group: Private Methods
    // _____________________________________
    // method: isProduction
    // Returns true if code runs in production
    static bool
    isProduction();
};
#endif