#include "Logger.h"

static bool production = false;

Logger::Logger(){};

void Logger::begin(unsigned long baud)
{
    if (!isProduction())
    {
        Serial.begin(baud);
    }
};

void Logger::println(String message)
{
    if (!isProduction())
    {
        Serial.println(message);
    }
};

void Logger::println(unsigned char message, int base)
{
    if (!isProduction())
    {
        Serial.println(message, base);
    }
};

void Logger::println(const char *message)
{
    if (!isProduction())
    {
        Serial.println(message);
    }
};

void Logger::print(String message)
{
    if (!isProduction())
    {
        Serial.print(message);
    }
};

bool Logger::isProduction()
{
    return production;
};